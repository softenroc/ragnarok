package com.ragnarok.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.ragnarok.pos.model.Category;
import com.ragnarok.pos.service.ICategoryService;

class CategoryRepositoryTest {

	@Test
	void testFindCategoryById() {
		
		ICategoryService iCategoryServiceMock = mock(ICategoryService.class);
		
		when(iCategoryServiceMock.retrieveCategory(1)).thenReturn(new Category(1,"category","category",true));
		String name= iCategoryServiceMock.retrieveCategory(1).getName();
		System.out.println("iCategoryServiceMock.retrieveCategory.getName = "+name);
		assertEquals("category", name);
	}
	
}
