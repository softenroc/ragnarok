package com.ragnarok.pos.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.util.Arrays;

import javax.persistence.Entity;

import org.h2.util.New;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.ragnarok.pos.Application;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, 
			webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryControllerIT {
	
	@LocalServerPort
	private int port;
	
	TestRestTemplate testRestTemplate = new TestRestTemplate();
	
	HttpHeaders headers = new HttpHeaders(); // create

	 @Before
	    public void setupJSONAcceptType() {
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    }
	 
	@Test
	public void retrieveCategory() throws Exception  {	
		
		String expected = "{\"id\":1,\"name\":\"Tapabocas\",\"description\":\"Tapabocas para el rostro\",\"active\":true}";

		ResponseEntity<String> response = testRestTemplate.exchange(createUrl("/category/1"),
				HttpMethod.GET, new HttpEntity<String>(null,
                        headers), String.class);
		System.out.println("***** RESPONSE STATUS : "+response.getStatusCodeValue()+" ******* RESPONSE BODY: "+response.getBody());
		JSONAssert.assertEquals(expected, response.getBody(), false);			
		
	}
	
	private String createUrl(String uri) {
        return "http://localhost:" + port + "/ragnarok"+uri;
    }

}
