package com.ragnarok.pos.controller;


import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ragnarok.pos.dto.CategoryDto;
import com.ragnarok.pos.model.Category;
import com.ragnarok.pos.service.ICategoryService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class CategoryController {	
	 
	@Autowired
	private ICategoryService categoryService;
	

	@GetMapping("/category/{categoryId}")
	@ResponseBody
	public ResponseEntity<CategoryDto> retrieveCategoryById(@PathVariable long categoryId) {
		CategoryDto categoryDto = convertToDto(categoryService.retrieveCategory(categoryId));
		return categoryDto != null ?  new ResponseEntity<>(categoryDto,HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/category")
	public List<Category> retrieveAllCategories() {
		return categoryService.retrieveAllCategories();
	}
	
	@DeleteMapping("/category/{categoryId}")
	public ResponseEntity<Object> deleteCategoryById(@PathVariable long categoryId) {
		if(Boolean.TRUE.equals(categoryService.deleteCategory(categoryId)))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		else {
			return new ResponseEntity<>("CategoryById "+categoryId+" not exist",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/category")
	public ResponseEntity<Object> updateArticle(@RequestBody CategoryDto categoryDto) {			
		if(Boolean.TRUE.equals(categoryService.updateCategory(convertToEntity(categoryDto))))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		else {
			return new ResponseEntity<>("updateArticle error",HttpStatus.BAD_REQUEST);
		}			
	}
	
	@ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
	@PostMapping("/category")
	public CategoryDto createCategory(@RequestBody CategoryDto categoryDto) {
		categoryDto.setActive(true);		
		log.info("Create category {} , {} ",categoryDto.getName(),categoryDto.getDescription());
		return convertToDto(categoryService.saveCategory(convertToEntity(categoryDto)));					
	}
		
	private CategoryDto convertToDto(Category category) {	
		return category!=null ? new ModelMapper().map(category, CategoryDto.class) : null;		
	}
	
	private Category convertToEntity(CategoryDto categoryDto) {		
		return categoryDto!=null ? new ModelMapper().map(categoryDto, Category.class) : null;		
	}
			
}