package com.ragnarok.pos.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ragnarok.pos.configuration.BasicConfiguration;
import com.ragnarok.pos.service.WelcomeService;

@RestController
public class WelcomeController {
	
	@Autowired
	private BasicConfiguration configuration;

	@RequestMapping("/welcome")
	public String welcome() {
		return WelcomeService.MESSAGE;
	}
	
	@RequestMapping("/dynamic-configuration")
	public Map<String, Object> dynamicConfiguration() {		
		   Map<String, Object> map = new HashMap<>();
           map.put("message", configuration.getMessage());
           map.put("number", configuration.getNumber());
           map.put("key", configuration.isValue());
           return map;		
	}
}
