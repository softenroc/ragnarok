package com.ragnarok.pos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true) 
@Getter
@Setter
@Table(name = "category")
@Entity
public class Category implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="id")
	private long id;
	
	@Column(name="name", columnDefinition = "varchar(100) NOT NULL")	
	private String name;
	
	@Column(name="description", columnDefinition = "varchar(255) NOT NULL")	
	private String description;
	
	@Column(name="active", columnDefinition = "boolean NOT NULL DEFAULT TRUE")	
	private boolean active;
	
    public Category() {
    }

    public Category(int id, String name, String description, boolean active ) {
        this.id = id;
        this.name = name;
        this.description= description;
        this.active = active;
    }
	
}
