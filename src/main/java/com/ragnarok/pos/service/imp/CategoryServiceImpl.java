package com.ragnarok.pos.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ragnarok.pos.model.Category;
import com.ragnarok.pos.service.ICategoryService;
import com.ragnarok.repository.CategoryRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CategoryServiceImpl implements ICategoryService{
				
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> retrieveAllCategories() {
		return (List<Category>) categoryRepository.findAll();		
	}
	
	@Override
	public Category retrieveCategory(long categoryId) {				
		return  categoryRepository.findById(categoryId).orElse(null);
	}
	
	@Override
	public Category saveCategory(Category category) {
		  try {		
			  log.info("saveCategory OK id = {}",category.getId());
			  return categoryRepository.save(category);			               
         } catch (Exception e) {
	          log.error(" erro in saveCategory : {}",e.toString());
	          return null;
         }		
	}
	
	@Override
	public Boolean updateCategory(Category category) {	
		categoryRepository.save(category);
		return true;
	}	
 
	@Override
	public Boolean deleteCategory(long categoryId) {
			if(categoryRepository.findById(categoryId).isPresent()) {
				categoryRepository.deleteById(categoryId);
				return true;
			}
			else {
				 log.info("deleteCategory id = {} not exist",categoryId);
				 return false;
			}
	}	
	
}