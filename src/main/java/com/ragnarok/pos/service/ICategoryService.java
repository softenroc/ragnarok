package com.ragnarok.pos.service;

import java.util.List;

import com.ragnarok.pos.model.Category;

public interface  ICategoryService {

	List<Category> retrieveAllCategories();
	Category retrieveCategory(long categoryId);
	Category saveCategory(Category category);
	Boolean deleteCategory(long categoryId);
	Boolean updateCategory(Category category);
	
}