package com.ragnarok.pos.service;

import org.springframework.stereotype.Service;

@Service
public class WelcomeService {

	public static final String MESSAGE = "WELCOME TO RAGNAROK API";	
	 private WelcomeService() {		    
		  }

}