package com.ragnarok.pos;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.ragnarok.repository"})
public class Application {  
    public static void main(String[] args) {
	 SpringApplication.run(Application.class, args);
    }
    
    @Profile("prod")
    @Bean
    public String dummy() {
    	return "something";
    }
} 