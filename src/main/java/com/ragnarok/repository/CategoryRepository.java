package com.ragnarok.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ragnarok.pos.model.Category;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long>{	

}
