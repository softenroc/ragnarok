-- 1.0 CREATE TABLE CATEGORY

CREATE TABLE category (
	id INT(11) NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) ,
	description VARCHAR(255) ,
	active boolean DEFAULT TRUE,
	PRIMARY KEY (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
